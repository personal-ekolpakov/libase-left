package kz.greetgo.libase.strureader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OracleRowReaderTest {
  
  private Connection connection;
  
  @BeforeClass
  private void beforeClass() throws Exception {
    Class.forName("oracle.jdbc.OracleDriver");
    
    connection = DriverManager.getConnection(//
        "jdbc:oracle:thin:@192.168.0.101:1521:orcl", //
        "aaa1", "aaa1");
  }
  
  @AfterClass
  private void afterClass() throws Exception {
    connection.close();
    connection = null;
  }
  
  @Test
  public void readAllTableColumns() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    List<ColumnRow> list = reader.readAllTableColumns();
    
    for (ColumnRow columnRow : list) {
      System.out.println(columnRow);
    }
  }
  
  @Test
  public void readAllTablePrimaryKey() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    Map<String, PrimaryKeyRow> map = reader.readAllTablePrimaryKeys();
    
    for (PrimaryKeyRow primaryKey : map.values()) {
      System.out.println(primaryKey);
    }
  }
  
  @Test
  public void readAllForeignKeys() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    for (ForeignKeyRow r : reader.readAllForeignKeys().values()) {
      System.out.println(r);
    }
  }
  
  @Test
  public void readAllSequences() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    for (SequenceRow x : reader.readAllSequences().values()) {
      System.out.println(x);
    }
    
  }
  
  @Test
  public void readAllViews() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    for (ViewRow vr : reader.readAllViews().values()) {
      System.out.println(vr);
    }
  }
  
  @Test
  public void readAllFuncs() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    for (StoreFuncRow f : reader.readAllFuncs()) {
      System.out.println(f);
    }
  }
  
  @Test
  public void readAllTriggers() throws Exception {
    RowReaderOracle reader = new RowReaderOracle(connection);
    for (TriggerRow x : reader.readAllTriggers().values()) {
      System.out.println(x);
    }
  }
}
